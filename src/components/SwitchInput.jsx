import React, { useState } from 'react';
import { Box, FormControlLabel, Switch } from '@mui/material';

const SwitchInput = () => {
    const [checked, setchecked] = useState(false);

    console.log(checked);

    const changeSwitch = (event) => {
        setchecked(event.target.checked);
    }

    return (
        <Box>
            <FormControlLabel label="Dark Mode" control={<Switch
                checked={checked}
                onChange={changeSwitch}
                color="error"
                size="small"
            />} />

        </Box>
    )
}

export default SwitchInput;